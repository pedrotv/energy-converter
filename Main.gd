extends Node2D

var gem = preload("res://Gem.tscn")

var selectedGem = null

signal swiped(direction)
signal swiped_canceled(start_position)

onready var swipeTimer = $SwipeTimer

var swipe_start_position = Vector2()

export (float, 1.0, 1.5) var MAX_DIAGONAL_SLOPE = 1.3

var grid = []

func _ready():
	for x in 6:
		grid.append([])
		for y in 5:
			grid[x].append(find_node("Gem"+str(6 * y + x + 1)))
			grid[x][y].collum = x
	
	pass

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		printGemArray()
	
	#Move gem
	if selectedGem != null:
		if Input.is_action_just_pressed("ui_left") && getGemPosition().x > 0:
			#Move gem to the side
			moveGem(-1)
			
		if Input.is_action_just_pressed("ui_right") && getGemPosition().x < 5:
			#Move gem to the side
			moveGem(+1)
	pass

func moveGem(var pol):
	var g = getGemPosition()
	#If there's a gem to the side, trade places
	if (g.y < grid[g.x+pol].size()):
		#Trade positions
		var gemPos = Vector2(grid[g.x][g.y].position.x,grid[g.x][g.y].position.y)
		grid[g.x][g.y].position = grid[g.x+pol][g.y].position
		grid[g.x+pol][g.y].position = gemPos
		#Trade place on array
		var gemPlace = grid[g.x][g.y]
		grid[g.x][g.y] = grid[g.x+pol][g.y]
		grid[g.x+pol][g.y] = gemPlace
	else: #Gem falls on other collum
		grid[g.x][g.y].position = Vector2(grid[g.x][g.y].position.x + pol * 64, grid[g.x][g.y].position.y)
		grid[g.x][g.y].collum += pol
		for a in grid[g.x].size() - g.y:
			grid[g.x][g.y].fall = true
			grid[g.x].remove(g.y) 
	selectedGem = null

func printGemArray():
	for x in 6:
		print("Collum " + str(x) + " - Size: "+ str(grid[x].size()))
		for y in grid[x].size():
			if grid[x][y].colorID == 0:
				print("Gem Color: Blue")
			elif grid[x][y].colorID == 1:
				print("Gem Color: Green")
			elif grid[x][y].colorID == 2:
				print("Gem Color: Red")
			elif grid[x][y].colorID == 3:
				print("Gem Color: Yellow")
			elif grid[x][y].colorID == 4:
				print("Gem Color: Purple")
			elif grid[x][y].colorID == 5:
				print("Gem Color: Orange")
			elif grid[x][y].colorID == 6:
				print("Gem Color: Pink")
			elif grid[x][y].colorID == 7:
				print("Gem Color: Cyan")

func getGemPosition():
	for x in 6:
		for y in grid[x].size():
			if grid[x][y] == selectedGem:
				return Vector2(x,y)
	pass

#Spawn gem
func _on_GemSpawnTimer_timeout():
	var r = randi()%6
	var g = gem.instance()
	g.set_position(Vector2(32 + (r*64),-64))
	add_child(g)
	g.collum = r
	g.firstFall = true
	g.fall = true
	pass

##Touch
#func _input(event):
#	if not event is InputEventScreenTouch || not event is InputEventMouseButton:
#		return
#	if event.pressed:
#		_start_detection(event.position)
#	elif not swipeTimer.is_stopped():
#		_end_detection(event.position)
#
#	pass
#
##Detect swipe
#func _start_detection(position):
#	swipe_start_position = position
#	swipeTimer.start()
#	pass
#
##Detect swipe
#func _end_detection(position):
#	swipeTimer.stop()
#	var direction = (position - swipe_start_position).normalized()
#	if abs(direction.x) + abs (direction.y) > MAX_DIAGONAL_SLOPE:
#		return
#
#	#Horizontal swipe
#	if abs(direction.x) > abs(direction.y):
#		emit_signal('swiped',Vector2(-sign(direction.x),0.0))
#	else:
#		emit_signal('swiped',Vector2(0.0, -sign(direction.y)))
#
#	pass
#
##Swipe timeout
#func _on_SwipeTimer_timeout():
#	emit_signal('swiped_canceled',swipe_start_position)
#	pass
#
#func _on_Main_swiped(direction):
#	print("Swiped! - Direction: "+ direction)
#	pass # Replace with function body.
#
#func _on_Main_swiped_canceled(start_position):
#	print("Swipe canceled! - Start Position:" + start_position)
#	pass # Replace with function body.
