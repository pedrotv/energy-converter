extends KinematicBody2D

onready var icon = $icon
onready var anim = $AnimationPlayer

export (int) var colorID

signal swipe

var object_hit
var direction = "s"
var swipe_start = Vector2()
var minimum_drag = 0
var moving = true
var gravity = Vector2(0,85)
var firstFall = false
var fall = false
var collum

func _ready():
	colorID = randi()%8
	
	#Set color
	if colorID == 0:
		icon.modulate = Color(0,0,1, 1) #Blue
	elif colorID == 1:
		icon.modulate = Color(0, 1, 0, 1) #Green
	elif colorID == 2:
		icon.modulate = Color(1,0,0, 1) #Red
	elif colorID == 3:
		icon.modulate = Color(1,1,0, 1) #Yellow
	elif colorID == 4:
		icon.modulate = Color(0.5, 0, 0.5, 1) #Purple
	elif colorID == 5:
		icon.modulate = Color(1, 0.5, 0, 1) #Orange
	elif colorID == 6:
		icon.modulate = Color(1, 0.75, 0.8, 1) #Pink
	elif colorID == 7:
		icon.modulate = Color(0,1,1, 1) #Cyan
	pass

func _process(delta):
	if fall:
		var collision = move_and_collide(gravity*delta)
		
		if collision:
			if collision.collider.get("fall") == false && collision.collider.get("collum") == collum: #If collided with already fallen gem
				fall = false
				var deleted = false
				
				#When the gem is falling
				if firstFall:
					#If has a gem of the same color, delete them
					for x in get_parent().grid[collum].size():
						if !deleted && get_parent().grid[collum][get_parent().grid[collum].size()-1-x].colorID == colorID:
							deleted = true
							get_parent().grid[collum][get_parent().grid[collum].size()-1-x].queue_free()
							get_parent().grid[collum].remove(get_parent().grid[collum].size()-1-x)
							for y in x: #Delete gems in between
								get_parent().grid[collum].pop_back().queue_free()
					firstFall = false
				
				#Delete gem
				if deleted:
					self.queue_free()
				else: #Don't delete and add to stack
					get_parent().grid[collum].append(self)
			elif collision.collider.name == "Floor":
				print("hit floor")
				fall = false
				get_parent().grid[collum].append(self)
	pass

func _on_Gem_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if fall == false:
				get_parent().selectedGem = self
	#			if colorID == 0:
	#				print("Obj Click - Gem Color: Blue")
	#			elif colorID == 1:
	#				print("Obj Click - Gem Color: Green")
	#			elif colorID == 2:
	#				print("Obj Click - Gem Color: Red")
	#			elif colorID == 3:
	#				print("Obj Click - Gem Color: Yellow")
	#			elif colorID == 4:
	#				print("Obj Click - Gem Color: Purple")
	#			elif colorID == 5:
	#				print("Obj Click - Gem Color: Orange")
	#			elif colorID == 6:
	#				print("Obj Click - Gem Color: Pink")
	#			elif colorID == 7:
	#				print("Obj Click - Gem Color: Cyan")
				#swipe_start = get_global_mouse_position()
			#else:
				#print("Obj Release - Gem "+str(colorID))
				#_calculate_swipe(get_global_mouse_position())
	pass

#func _calculate_swipe(swipe_end):
#	if swipe_start == null || swipe_start.x == 0: 
#		print("Swipe null! " + str(swipe_start.x))
#		return
#	# var swipe = swipe_end - swipe_start
#	print("Swipe: " + str(swipe_start.x) + " - " +str(swipe_end.x))
#	#if abs(swipe.x) > minimum_drag:
#
#	if swipe_start.x < swipe_end.x:
#		#emit_signal("swipe")
#		direction = "right"
#
#	else:
#		#emit_signal("swipe")
#		direction = "left"
#	print(direction)

